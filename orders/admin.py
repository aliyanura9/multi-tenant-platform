from django.contrib import admin
from orders.models import Order, OrderProduct


class OrderProductInline(admin.TabularInline):
    model = OrderProduct
    extra = 0


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('client_info', 'address', 'is_delivered',
                    'amount', 'is_paid', 'created_at')
    list_display_links = ('client_info',)
    list_filter = ('address', 'is_delivered',
                   'amount', 'is_paid', 'created_at')
    list_editable = ('is_delivered', 'is_paid')
    inlines = (OrderProductInline,)
