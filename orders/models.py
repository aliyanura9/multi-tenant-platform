from django.db import models
from products.models import Product
from users.models import Tenant


class Order(models.Model):
    client_info = models.TextField(verbose_name='Информация о клиенте')
    address = models.CharField(max_length=250,
                               verbose_name='Адрес доставки')
    amount = models.FloatField(verbose_name='Сумма заказа')
    is_paid = models.BooleanField(default=False, verbose_name='Оплачено')
    is_delivered = models.BooleanField(default=False, verbose_name='Доставлено')
    created_at = models.DateTimeField(auto_now=True, verbose_name='Дата создания')
    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE,
                               related_name='orders',
                               verbose_name='Арендатор')

    class Meta:
        db_table = 'orders'
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ('-created_at', 'is_paid', 'is_delivered')


class OrderProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                verbose_name='Продукт')
    order = models.ForeignKey(Order, on_delete=models.CASCADE,
                              related_name='products',
                              verbose_name='Заказ')
    
    class Meta:
        db_table = 'order_products'
        verbose_name = 'Продукт заказа'
        verbose_name_plural = 'Продукты заказа'