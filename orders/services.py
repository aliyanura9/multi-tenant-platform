from django.db.transaction import atomic
from users.exceptions import ObjectNotFoundException
from orders.models import Order, OrderProduct


class OrderProductService:
    model = OrderProduct

    @classmethod
    def create(cls, order, product):
        cls.model.objects.create(
            order=order,
            product=product
        )


class OrderService:
    model = Order

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')

    @classmethod
    def filter(cls, *args, **kwargs):
        return cls.model.objects.filter(**kwargs)

    @classmethod
    @atomic
    def create(cls, tenant, client_info: str,
               address: str, is_paid: bool,
               is_delivered: bool, products: list):
        amont = sum([i['product'].price for i in products])
        order = cls.model.objects.create(
            tenant=tenant,
            client_info=client_info,
            address=address,
            is_paid=is_paid,
            is_delivered=is_delivered,
            amount=amont
        )
        [OrderProductService.create(order, i['product']) for i in products]

    @classmethod
    @atomic
    def update(cls, id, client_info: str,
               address: str, is_paid: bool,
               is_delivered: bool, products: list):
        order = cls.get(id=id)
        amount = sum([i['product'].price for i in products])
        order.client_info = client_info
        order.address = address
        order.is_paid = is_paid
        order.is_delivered = is_delivered
        order.amount = amount
        order.save()
        order.products.all().delete()
        [OrderProductService.create(order, i['product']) for i in products]

