from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.viewsets import ModelViewSet
from products.permissions import IsTenant
from orders.serializers import OrderSerializer
from orders.services import OrderService


class OrderViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated, IsTenant)
    authentication_classes = (TokenAuthentication,)
    serializer_class = OrderSerializer
    lookup_field = 'pk'

    def get_queryset(self):
        return OrderService.filter(tenant=self.request.user.tenant)
    
    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        OrderService.create(
            tenant=request.user.tenant,
            client_info=serializer.validated_data.get('client_info'),
            address=serializer.validated_data.get('address'),
            is_paid=serializer.validated_data.get('is_paid', False),
            is_delivered=serializer.validated_data.get('is_delivered', False),
            products=serializer.validated_data.get('products'),
        )
        return Response(data={
            'message': 'The order has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)
    
    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        OrderService.update(
            id=kwargs.get('pk'),
            client_info=serializer.validated_data.get('client_info'),
            address=serializer.validated_data.get('address'),
            is_paid=serializer.validated_data.get('is_paid', False),
            is_delivered=serializer.validated_data.get('is_delivered', False),
            products=serializer.validated_data.get('products'),
        )
        return Response(data={
            'message': 'The order has been successfully updated',
            'status': 'OK'
        }, status=status.HTTP_200_OK)
