from rest_framework import serializers
from orders.models import Order, OrderProduct


class OrderProductSerializer(serializers.ModelSerializer):
    product_name = serializers.CharField(source='product.name', required=False)
    price = serializers.FloatField(source='product.price', required=False)
    class Meta:
        model = OrderProduct
        fields = ('product', 'product_name', 'price')



class OrderSerializer(serializers.ModelSerializer):
    products = OrderProductSerializer(many=True)
    class Meta:
        model = Order
        fields = ('client_info', 'address', 'is_delivered',
                  'amount', 'is_paid', 'products')
        extra_kwargs = {
            'amount': {'required': False}
        }