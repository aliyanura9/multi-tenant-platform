from django.urls import path
from orders.views import OrderViewSet

urlpatterns = [
    path('orders/', OrderViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('orders/<int:pk>/', OrderViewSet.as_view({'put': 'update', 'delete': 'destroy'})),
]