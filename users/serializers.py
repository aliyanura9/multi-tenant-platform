from rest_framework import serializers
from users.models import User, Tenant


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(min_length=2, required=True)
    password = serializers.CharField(min_length=2, required=True)


class TenantSerializer(serializers.ModelSerializer):
    user = LoginSerializer(many=False, required=False)

    class Meta:
        model = Tenant
        fields = ('id', 'first_name', 'last_name', 'user')

class TenantListSerializer(serializers.ModelSerializer):
    email = serializers.CharField(source='user.email')

    class Meta:
        model = Tenant
        fields = ('id', 'first_name', 'last_name', 'email')