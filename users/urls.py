from django.urls import path
from users.views import LoginViewSet, TenantViewSet

urlpatterns = [
    path('login/', LoginViewSet.as_view({'post': 'post'})),
    path('tenants/', TenantViewSet.as_view({'get': 'list', 'post':'create'})),
    path('tenants/<int:pk>/', TenantViewSet.as_view({'put':'update', 'delete': 'destroy'})),
]