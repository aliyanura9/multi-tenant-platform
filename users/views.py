from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.authentication import TokenAuthentication
from users.serializers import LoginSerializer, TenantSerializer,\
                              TenantListSerializer
from users.services import TokenService, TenantService
class LoginViewSet(ModelViewSet):
    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = TokenService.create_auth_token(
            email=serializer.validated_data.get('email'),
            password=serializer.validated_data.get('password')
        )
        return Response(data={
            'message': 'You have successfully logged in',
            'data': {
                'token': str(token),
                'token_type': 'Token',
                'user_id': user.pk,
            },
            'status': "OK"
        }, status=status.HTTP_200_OK)


class TenantViewSet(ModelViewSet):
    permission_classes = (IsAdminUser,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = TenantSerializer
    lookup_field = 'pk'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return TenantListSerializer
        return super().get_serializer_class()

    def list(self, request, *args, **kwargs):
        tenants = TenantService.filter()
        data = self.get_serializer_class()(tenants, many=True).data
        return Response(data={
            'results': data,
            'status': 'OK'
        }, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        TenantService.create(
            first_name=serializer.validated_data.get('first_name'),
            last_name=serializer.validated_data.get('last_name'),
            user=serializer.validated_data.get('user'),
        )
        return Response(data={
            'message': 'The tenant has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        TenantService.update(
            id=kwargs.get('pk'),
            first_name=serializer.validated_data.get('first_name'),
            last_name=serializer.validated_data.get('last_name'),
            user=serializer.validated_data.get('user', None),
        )
        return Response(data={
            'message': 'The tenant has been successfully updated',
            'status': 'OK'
        }, status=status.HTTP_200_OK)
    
    def destroy(self, request, *args, **kwargs):
        TenantService.delete(
            id=kwargs.get('pk'),
        )
        return Response(data={
            'message': 'The tenant has been successfully deleted',
            'status': 'NO CONTENT'
        }, status=status.HTTP_204_NO_CONTENT)
