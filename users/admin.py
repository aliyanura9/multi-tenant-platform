from django.contrib import admin
from users.models import User, Tenant


class TenantInline(admin.StackedInline):
    model = Tenant
    fields = ('first_name', 'last_name',)
    readonly_fields = ('created_at', 'updated_at')


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'is_staff', 'is_superuser',
                    'is_active', 'created_at')
    list_display_links = ('email',)
    list_editable = ('is_staff', 'is_superuser', 'is_active')
    list_filter = ('email', 'is_staff', 'is_superuser',
                    'is_active', 'created_at')
    fields = ('email', 'is_staff', 'is_superuser',
              'is_active')
    readonly_fields = ('created_at',)
    inlines = (TenantInline,)


@admin.register(Tenant)
class TenantAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'last_name',
                    'created_at')
    list_display_links = ('first_name', 'last_name')
    list_filter = ('user', 'first_name', 'last_name', 'created_at')
    fields = ('user', 'first_name', 'last_name',)
    readonly_fields = ('created_at', 'updated_at')
