from django.contrib.auth.models import (
        AbstractBaseUser, PermissionsMixin
    )
from users.managers import CustomUserManager
from django.db import models


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(blank=False, null=False, unique=True,
                              verbose_name='почтовый адрес')
    password = models.CharField(max_length=128, blank=False, null=False,
                                verbose_name='пароль')
    is_staff = models.BooleanField(default=False, blank=True,
                                   verbose_name='сотрудник системы')
    is_superuser = models.BooleanField(default=False, blank=True,
                                       verbose_name='администратор')
    is_active = models.BooleanField(default=False, blank=True,
                                    verbose_name='активный')
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name='дата создания')

    USERNAME_FIELD = 'email'

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    class Meta:
        db_table = 'users'
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ('-created_at',)


class Tenant(models.Model):
    first_name = models.CharField(max_length=25, blank=False, null=False,
                                  verbose_name='имя')
    last_name = models.CharField(max_length=25, blank=False, null=False,
                                 verbose_name='фамилия')
    user = models.OneToOneField(User, unique=True, blank=False, null=False,
                                on_delete=models.CASCADE,
                                related_name='tenant',
                                verbose_name='пользователь')
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name='дата создания')
    updated_at = models.DateTimeField(blank=True, null=True,
                                      verbose_name='дата изменения')

    def __str__(self):
        return f'{self.last_name} {self.first_name}'.title()

    class Meta:
        db_table = 'tenants'
        verbose_name = 'Арендатор'
        verbose_name_plural = 'Арендаторы'
        ordering = ('created_at',)
