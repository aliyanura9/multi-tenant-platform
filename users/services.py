from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from users.models import User, Tenant
from users.exceptions import ObjectNotFoundException


class TokenService:
    model = Token

    @classmethod
    def create_auth_token(cls, email: str, password: str):
        user = authenticate(username=email, password=password)
        if user:
            token, created = cls.model.objects.get_or_create(user=user)
            return user, token
        else:
            raise ObjectNotFoundException('User not found or not active')



class UserService:
    model = User

    @classmethod
    def create(cls, email: str, password: str):
        return cls.model.objects.create_user(
            email=email,
            password=password,
        )
    
    @classmethod
    def update(cls, user, email: str, password: str):
        user.email=email
        user.set_password(password)
        user.save()


class TenantService:
    model = Tenant

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')
        
    @classmethod
    def filter(cls, *args, **kwargs):
        return cls.model.objects.filter(**kwargs)

    @classmethod
    def create(cls, first_name: str, last_name: str, user):
        tenant_user = UserService.create(
            email=user['email'],
            password=user['password']
        )
        cls.model.objects.create(
            first_name=first_name,
            last_name=last_name,
            user=tenant_user
        )
    
    @classmethod
    def update(cls, id: int, first_name: str,
               last_name: str, user):
        tenant = cls.get(id=id)
        tenant_user = tenant.user
        if user:
            UserService.update(
                user=tenant_user,
                email=user['email'],
                password=user['password']
            )
        tenant.first_name=first_name
        tenant.last_name=last_name
        tenant.save()

    @classmethod
    def delete(cls, id: int):
        tenant = cls.get(id=id)
        user = tenant.user
        user.delete()
