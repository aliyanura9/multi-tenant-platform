from rest_framework.permissions import BasePermission
from users.models import Tenant

class IsTenant(BasePermission):
    def has_permission(self, request, view):
        if Tenant.objects.filter(user=view.request.user).exists():
            return True
        return False