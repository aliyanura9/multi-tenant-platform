from django.urls import path
from products.views import CategoryViewSet, SizeViewSet,\
                           ProductViewSet

urlpatterns = [
    path('categories/', CategoryViewSet.as_view({'get': 'list'})),
    path('sizes/', SizeViewSet.as_view({'get': 'list'})),
    path('products/', ProductViewSet.as_view({'get': 'list', 'post':'create'})),
    path('products/<int:pk>/', ProductViewSet.as_view({'put':'update', 'delete': 'destroy'})),
]