from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.viewsets import ModelViewSet
from products.serializers import ProductSerializer,\
                                 CategorySerializer,\
                                 SizeSerializer
from products.models import Category, Size,Product
from products.permissions import IsTenant
from products.services import ProductService


class CategoryViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class SizeViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = SizeSerializer
    queryset = Size.objects.all()


class ProductViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated, IsTenant)
    authentication_classes = (TokenAuthentication,)
    serializer_class = ProductSerializer
    lookup_field = 'pk'

    def get_queryset(self):
        return Product.objects.filter(tenant=self.request.user.tenant)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        ProductService.create(
            tenant=request.user.tenant,
            name=serializer.validated_data.get('name'),
            description=serializer.validated_data.get('description'),
            price=serializer.validated_data.get('price'),
            image=serializer.validated_data.get('image'),
            category=serializer.validated_data.get('category'),
            color=serializer.validated_data.get('color'),
            size=serializer.validated_data.get('size'),
        )
        return Response(data={
            'message': 'The tenant has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)
    
    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        ProductService.update(
            id=kwargs.get('pk'),
            name=serializer.validated_data.get('name'),
            description=serializer.validated_data.get('description'),
            price=serializer.validated_data.get('price'),
            image=serializer.validated_data.get('image'),
            category=serializer.validated_data.get('category'),
            color=serializer.validated_data.get('color'),
            size=serializer.validated_data.get('size'),
        )
        return Response(data={
            'message': 'The tenant has been successfully update',
            'status': 'OK'
        }, status=status.HTTP_200_OK)


