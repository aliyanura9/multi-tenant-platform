from rest_framework import serializers
from products.models import Product, Category, Size


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = ('id', 'name')


class ProductSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category.name',
                                          required=False)
    size_name = serializers.CharField(source='size.name',
                                      required=False)
    color_name = serializers.CharField(source='get_color_display',
                                      required=False)
    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'price',
                  'image', 'category', 'category_name',
                  'color', 'color_name', 'size', 'size_name')
