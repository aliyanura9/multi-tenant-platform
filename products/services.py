from products.models import Product
from users.exceptions import ObjectNotFoundException


class ProductService:
    model = Product

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')

    @classmethod
    def create(cls, tenant, name: str, description: str,
               price: float, image: str, category,
               color: str, size):
        cls.model.objects.create(
            tenant=tenant,
            name=name,
            description=description,
            price=price,
            image=image,
            category=category,
            color=color,
            size=size
        )
    
    @classmethod
    def update(cls, id: str, name: str, description: str,
               price: float, image: str, category,
               color: str, size):
        product = cls.get(id=id)
        product.name=name
        product.description=description
        product.price=price
        product.image=image
        product.category=category
        product.color=color
        product.size=size
        product.save()
