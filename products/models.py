from django.db import models
from users.models import Tenant


class Category(models.Model):
    name = models.CharField(max_length=250, verbose_name='Наименование')

    def __str__(self) -> str:
        return self.name

    class Meta:
        db_table = 'categories'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('name',)


class Size(models.Model):
    name = models.CharField(max_length=250, verbose_name='Наименование')

    def __str__(self) -> str:
        return self.name

    class Meta:
        db_table = 'sizes'
        verbose_name = 'Размер'
        verbose_name_plural = 'Размеры'
        ordering = ('name',)


class Product(models.Model):
    COLOR_CHOICE = (
        ('1', 'Черный'),
        ('2', 'Белый'),
        ('3', 'Красный'),
        ('4', 'Желтый'),
        ('5', 'Оранжевый'),
        ('6', 'Зеленый'),
        ('7', 'Голубой'),
        ('8', 'Синий'),
        ('9', 'Фиолетовый'),
        ('10', 'Розовый'),
        ('11', 'Коричневый'),
        ('12', 'Серый'),
    )
    name = models.CharField(max_length=250, verbose_name='Наименование')
    description = models.TextField(verbose_name='Описание')
    price = models.FloatField(verbose_name='Цена')
    image = models.ImageField(verbose_name='Изображение', upload_to='products/')
    category = models.ForeignKey(Category, on_delete=models.SET_NULL,
                                 null=True, verbose_name='Категория')
    color = models.CharField(choices=COLOR_CHOICE, max_length=10,
                             verbose_name='Цвет')
    size = models.ForeignKey(Size, on_delete=models.SET_NULL,
                             null=True, verbose_name='Размер')
    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE,
                               related_name='products',
                               verbose_name='Арендатор')
    
    def __str__(self) -> str:
        return self.name

    class Meta:
        db_table = 'products'
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        ordering = ('name',)
